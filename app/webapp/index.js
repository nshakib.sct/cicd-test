sap.ui.define([
	"sap/ui/core/ComponentContainer"
], function (ComponentContainer) {
	"use strict";

	new ComponentContainer({
		name: "demo.fullstack.sap",
		settings : {
			id : "fullstack"
		},
		async: true
	}).placeAt("content");
});