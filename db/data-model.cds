namespace my.product;
entity Products {
        key ProductID       : Integer;
            ProductName     : String;
            SupplierID      : Integer;
            CategoryID      : Integer;
            QuantityPerUnit : String;
            UnitPrice       : String;
            UnitsInStock    : Integer;
            UnitsOnOrder    : Integer;
            ReorderLevel    : Integer;
            Discontinued    : Boolean;

    }